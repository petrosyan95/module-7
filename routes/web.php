<?php

use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Test;
use App\Http\Controllers\TextController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/user', [UsersController::class, 'create'])->middleware('user');
//Route::post('/user', [UsersController::class, 'store']);
//Route::get('/user/{id}', [UsersController::class, 'show']);
//Route::put('/user/{id}', [UsersController::class, 'update']);
//Route::delete('/user/{id}', [UsersController::class, 'destroy']);
Route::get('/user', [UsersController::class]);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/telegraph', [TextController::class, 'withdraw'])->name('GET');
Route::post('/telegraph', [TextController::class, 'add'])->name('POST');
Route::put('/telegraph', [TextController::class, 'update'])->name('PUT');
Route::delete('/telegraph', [TextController::class, 'delete'])->name('DELETE');
