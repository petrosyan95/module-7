<?php

namespace entities;

class FileStorage
{
    public function create(string $slug, array $arraySerialize) : string | bool
    {
        if ($slug !== null) {
            $slug = preg_replace("/ _ /", '_'.date('Y,m,d').' _0.txt', $slug);
            if (file_exists($slug)) {
                $underscore_position = strpos($slug, ' _');
                $implement = intval(substr($slug, $underscore_position + 2, -(strlen($slug) - strpos($slug, '.', -6)))) + 1;
                $slug = preg_replace('/\b( _[1-9]?[0-9]{1,2}| _0)\b/', ' _' . $implement, $slug);
                $this->create($slug, $arraySerialize);
            } else {
                file_put_contents($slug, serialize($arraySerialize));
            }
        } else {
            self::logMessage('create error напишите имя нового файла!');
        }
        return $slug;
    }

    public function read(string $slug) : object | bool
    {
        if (file_exists($slug)) {
            return json_decode( json_encode(unserialize(file_get_contents($slug))));
        } else {
            self::logMessage('read error Такого файла нет! Убедитесь в правильном написании чтобы получить обйект');
            return false;
        }
    }
    private $q=self::class;
    public function update(string $slug, array $array) : void
    {
        if (file_exists($slug)) {
            file_put_contents($slug, serialize($array));
            echo 'Файл удачно перезаписен' . PHP_EOL;
        } else {
            self::logMessage('update error Такого файла нет! Убедитесь в правильном написании чтобы обновить файл');

        }
    }

    public function delete(string $slug) : void
    {
        if (file_exists($slug)) {
            unlink($slug);
            echo 'Файл успешно удалён' . PHP_EOL;
        } else {
            self::logMessage('delete error Такого файла нет! Убедитесь в правильном написании чтобы удалить файл');
        }
    }

    public function list() : array | bool
    {
        $arrayText = [];
        if (is_array(glob('*_*'))) {
            foreach (glob('*_*') as $value) {
                $arrayText[] = unserialize(file_get_contents($value));
            }
        } else {
            self::logMessage('list error Для начало создайте файлы!');
        }
        return $arrayText;
    }

}
