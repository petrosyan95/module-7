<?php

namespace entities;

use interfaces\EventListenerInterface as EventListenerInterface;

abstract class User implements EventListenerInterface
{
    protected $id;
    protected $name;
    protected $role;
    protected $array = [];

//выводит список текстов, доступных пользователю для редактирования.
    abstract function getTextsToEdit() : string;

    public function attachEvent (string $nameMethod, callable $function) : void
    {
        if (in_array($nameMethod, get_class_methods($this)) && !in_array($nameMethod, $this->array)) {
            call_user_func(array($this, $nameMethod));
        }else{
            echo 'ошибок такого метода нет или его не надо отслеживать';
        }
    }

    public function detouchEvent (string $nameMethod) : void
    {
        $this->array[] = $nameMethod;
    }
}