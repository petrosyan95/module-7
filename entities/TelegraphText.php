<?php

namespace entities;

class TelegraphText extends FileStorage
{
    private $title;

    private $text;
    private $author;
    private $published;
    private $slug;

    public function __set(string $name, string $value): void
    {
        if (array_key_exists($name, get_object_vars($this))){
            ($name === 'author' && mb_strlen($value) <= 120) ? $this->$name = $value :
                ($name === 'slug' && preg_match('/^[a-zA-Z0-9_-]+$/', $value )? $this->$name = $value :
                    ($name === 'published' && $name <= date('Y.m.d') ? $this->$name = $value : null));


        }
    }

    public function __get(string $name)
    {
        if ($name === 'text'){
            $this->loadText();
            $this->storeText();
        }
    }


    public function __construct(string $author, string $slug)
    {
        $this->author = $author;
        $this->slug = $slug;
        $this->published = date('Y.m.d  H:i:s');
    }

    private function storeText()
    {
        $arraySerialize = [
            "text" => $this->text,
            "title" => $this->title,
            "autor" => $this->author,
            "published" => $this->published
        ];
        $this->create($this->slug." _ ", $arraySerialize);
    }

    private function loadText(): string
    {
        if (file_exists($this->slug) && filesize($this->slug)) {
            $unserialize = unserialize(file_get_contents($this->slug));
            return PHP_EOL . $this->author = $unserialize['autor'] .
                    PHP_EOL . $this->title = $unserialize['title'] .
                        PHP_EOL . $this->text = $unserialize['text'] .
                            PHP_EOL . $this->published = $unserialize['published'];
        } else {
            return 'файл пуст или не создан!'. PHP_EOL;
        }

    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }

}

