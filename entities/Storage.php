<?php

namespace entities;

use interfaces\LoggerInterface as LoggerInterface;
use interfaces\EventListenerInterface as EventListenerInterface;

abstract class Storage implements LoggerInterface, EventListenerInterface
{
//create получает на вход объект, который нужно сохранить, и после сохранения возвращает уникальный идентификатор;
    abstract function create(string $slug, array $arraySerialize) : string | bool;

//read — получить объект из хранилища. Метод принимает на вход id или slug (любое из значений) объекта и возвращает объект;
    abstract function read(string $slug) : object | bool;
//update — обновить существующий объект в хранилище. Метод принимает на вход три параметра: id или slug (любое из значений)
// существующего объекта и сам обновлённый объект, который нужно сохранить;
    abstract function update(string $slug, array $array) : void;

//delete — удалить объект из хранилища. Метод принимает на вход id или slug объекта, который нужно удалить из хранилища;
    abstract function delete(string $slug) : void;
//list — возвращает массив всех объектов в хранилище.
    abstract function list() : array | bool;

    public function logMessage (string $error) : void
    {
        try {
            throw new Exception($error);
        }catch (Exception $e){
            (!file_exists('log'))?mkdir('log'):null;
            file_put_contents('./log/log.txt', PHP_EOL. '---NEW ERROR--- -> '. date('Y.m.d'). PHP_EOL. $e->getMessage().','. PHP_EOL, FILE_APPEND);
        }
    }

    public function lastMessages (int $quantityError) : array | bool
    {
        $arrayReturn = [];
        $array = explode(',',file_get_contents('./log/log.txt'));
        if (count($array) > 0) {
            ($quantityError > count($array)) ? $quantityError = count($array) - 1 : $quantityError;
            for ($i = 0; $i < $quantityError; $i++) {
                $arrayReturn[] = $array[$i];
            }
            return $arrayReturn;
        }else{
            self::logMessage('lastMessages error ошибок не обнаружено это первая :)');
            return false;
        }
    }

    protected $array = [];

    public function attachEvent (string $nameMethod, callable $function) : void
    {
        if (in_array($nameMethod, get_class_methods($this)) && !in_array($nameMethod, $this->array)) {
            call_user_func(array($this, $nameMethod));
        }else{
            echo 'ошибок такого метода нет или его не надо отслеживать';
            self::logMessage('attachEvent error ошибок такого метода нет или его не надо отслеживать');
        }
    }

    public function detouchEvent (string $nameMethod) : void
    {
        $this->array[] = $nameMethod;
    }
}

