<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\FileStorage;
use App\Models\TelegraphText;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class TextController extends Controller
{
    /**
     * @param Request $request
     * @return void
     */
    public function add (Request $request)
    {
        $telegraph = new TelegraphText();
        $telegraph->fill($request->all());
        $telegraph->save();

        $view = view('form')->with(['res' => TelegraphText::all()]);
        return new Response($view);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function update (Request $request)
    {
        $telegraph = TelegraphText::find($request->get('id'));
        $telegraph->update($request->all());

        $view = view('form')->with(['res' => TelegraphText::all()]);
        return new Response($view);
    }

    /**
     * @param $id
     * @return void
     */
    public function delete (Request $request)
    {
        TelegraphText::where('id', '=', $request->get('id'))->delete();

        $view = view('form')->with(['res' => TelegraphText::all()]);
        return new Response($view);
    }

    /**
     * @return Response
     */
    public function withdraw ()
    {
        $view = view('form')->with(['res' => TelegraphText::all()]);
        return new Response($view);
    }
}
