<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    *{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }
    .submit{
        width: 60px;
        height: 30px;
        padding: 5px 12px;
        border-radius: 10px;
        opacity: .6;
        margin-left: 15px;
        border: 0;
    }
    .submit:hover{
        transition: all .5s;
        opacity: 1;
    }
    .sql{
        background-color: rgb(112, 150, 230);
        width: 70%;
        margin: 5% auto;
        text-align: center;
        padding: 40px;
        border-radius: 20px;
    }
    .sql input, textarea{
        border-radius: 10px;
        border: 0;
        text-align: center;
        height: 30px;
        margin-left: 20px;
        opacity: .7;
        resize: none;
    }
    .sql input:hover{
        transition: all .5s;
        opacity: 1;
    }
    .sql h1 {
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        margin-bottom: 80px;
    }
    .id{
        width: 60px;
        pointer-events: none;
    }
</style>
<body>
<div class="sql">
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>TITLE</th>
            <th>TEXT</th>
            <th>AUTHOR</th>
            <th>SLUG</th>
            <th>ACTIONS</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($res as $val)
            <form method="post" action="{{route(isset($_POST['submit'])?$_POST['submit']:'GET')}}">
                @method(isset($_POST['submit']) && ($_POST['submit'] == 'PUT' || $_POST['submit'] == 'DELETE') ? $_POST['submit'] : 'GET')
                <tr>
                    <td><input type="text" name="id" value="{{$val['id']}}" readonly class="id"></td>
                    <td><input type="text" name="title" value="{{$val['title']}}"></td>
                    <td><input type="text" name="text" value="{{$val['text']}}"></td>
                    <td><input type="text" name="author" value="{{$val['author']}}"></td>
                    <td><input type="text" name="slug" value="{{$val['slug']}}"></td>
                    <td>
                        <button type="submit" class="submit" name="submit" value="PUT">Edit</button>
                        <button type="submit" class="submit" name="submit" value="DELETE">Delete</button>
                    </td>
                </tr>
            </form>
                @endforeach
        </tbody>
    </table>
</div>
<form method="post" action="{{route('POST')}}">
    @csrf
    <div class="sql">
        <h1>insert</h1>
        <input type="text" placeholder="title" name="title" required>
        <input type="text" placeholder="text" name="text" required>
        <input type="text" placeholder="author" name="author" required>
        <input type="text" placeholder="slug" name="slug" required>

        <button type="submit" class="submit" name="POST">add</button>
    </div>
</form>
</body>
</html>
