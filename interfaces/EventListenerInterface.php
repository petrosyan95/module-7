<?php

namespace interfaces;

interface EventListenerInterface
{
    public function attachEvent (string $nameMethod, callable $function) : void;
    public function detouchEvent (string $nameMethod) : void;
}